Role Name
=========

A role that installs a Docker Swarm instance of WebODV

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
